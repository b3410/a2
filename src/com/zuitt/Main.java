package com.zuitt;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String[] fruits = new String[5];
        fruits[0] = "apple";
        fruits[1] = "avocado";
        fruits[2] = "banana";
        fruits[3] = "kiwi";
        fruits[4] = "orange";

        System.out.println("Which fruit would you like get the index of?: ");

        Scanner nameScanner = new Scanner(System.in);
        String fruitIndex = nameScanner.nextLine();
        int result = Arrays.binarySearch(fruits ,fruitIndex);
        System.out.println("the index of " + fruitIndex + " is " + result);

        ArrayList<String> friends = new ArrayList<>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");
        System.out.println("My Friends are: " + friends);

        HashMap<String, Integer> hygieneKit = new HashMap<>();

        hygieneKit.put("toothpaste", 15);
        hygieneKit.put("toothbrush", 20);
        hygieneKit.put("soap", 12);
        System.out.println("Our current inventory consists of: " + hygieneKit);

    }
}
